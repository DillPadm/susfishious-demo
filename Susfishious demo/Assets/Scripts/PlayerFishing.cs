using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class PlayerFishing : MonoBehaviour
{
    private PlayerInputMap playerInput;
    private InputAction movement;

    //private bool canFish = true;
    public bool isFishing = false;
    public bool isFishAncorReleased = false;
    public bool isFishNibbling = false;

    public GameObject prefabAncor;
    private GameObject AncorInstance;
    public float ancorDistance;

    public int fishBar = 0;
    public int fishBarMax = 100;
    public float fishTimerCD = 0.125f;
    public float fishTimer = 0.125f;
    private bool fishBarDir = false;

    private void Awake()
    {
        playerInput = new PlayerInputMap();
        playerInput.Enable();
        movement.Enable();
    }

    private void OnEnable()
    {
        movement = playerInput.Player.Movement;
        movement.Enable();

        playerInput.Player.ActFishing.performed += ActFishing;
        playerInput.Player.ActFishing.Enable();
    }

    private void OnDisable()
    {
        movement.Disable();
        playerInput.Player.ActFishing.Disable();
    }

    // Update is called once per frame
    void Update()
    {
        FishingBar(isFishing);
    }

    public void ActFishing(InputAction.CallbackContext context)
    {
        Debug.Log(context.ReadValue<float>());
        switch (isFishing)
        {
            case false: isFishing = true;break;
            case true: isFishing = false; ActFishingAncor(fishBar);break;
        }
    }

    public void ActFishingAncor(float dis)
    {
        if (AncorInstance != null)
        {
            Destroy(AncorInstance);
        } 
        
        AncorInstance = Instantiate(prefabAncor, new Vector2(-ancorDistance * dis, 0), Quaternion.identity);
        isFishAncorReleased = true;
    }

    private void FishingBar(bool a)
    {
        if (a == true)
        {
            fishTimer -= Time.deltaTime;

            if (fishTimer <= 0)
            {
                switch (fishBarDir)
                {
                    case false:
                        if (fishBar < fishBarMax)
                        {
                            fishBar++;
                        }
                        else
                        if (fishBar >= fishBarMax)
                        {
                            fishBarDir = true;
                        }
                        break;
                    case true:
                        if (fishBar > 0)
                        {
                            fishBar--;
                        }
                        else
                        if (fishBar <= 0)
                        {
                            fishBarDir = false;
                        }
                        break;
                }
                fishTimer = fishTimerCD;
            }
        }
    }
}
